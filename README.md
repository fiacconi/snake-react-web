# Snake - React Web

This repository contains a React.js web application (crated with [Create React App](https://github.com/facebook/create-react-app)) that implements the game Snake based on HTML canvas.
I tried to give it as much as possible the "Nokia 3310 Snake II" look & feel by mimiking the overall appearance.
Specifically, all the visualization are computed by scaling the actual canvas resolution (336 x 192) for the Nokia 3310 monitor resolution, namely 84 x 48 pixel.
It is possible to play using the arrow keys on the keyboard or the mouse with four directional button on the app (to support mobile experience).

The whole app is written with [React.js](https://reactjs.org/) and is made responsive with [Bootstrap](https://getbootstrap.com/) CSS and [reactstrap](https://reactstrap.github.io/).

## Repository structure

The repository is structured as follows.

```
snake-react-web/
|__ .gitignore
|__ package.json
|__ package-lock.json
|__ README.md
|__ public/
|   |__ favicon.ico
|   |__ icon-192.png
|   |__ icon-512.jpg
|   |__ index.html
|   |__ manifest.json
|__ src/
    |__ App.css
    |__ App.js
    |__ index.css
    |__ index.js
    |__ serviceWorker.js
    |__ snake/
        |__ constants.js
        |__ draw.js
        |__ handlers.js
        |__ index.js
        |__ logic.js
        |__ utils.js
```

## How to run the application

First, git-clone the repository to a local directory.
Then, enter the directory and run `npm install` to download and isntall all the Node dependencies.

To test the application locally, run `npm start` and got to [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

To obtain a production build, run `npm run build` to build the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.
The files are ready to be deployed, e.g. through a static file server.
