import {React, useState} from "react";
import {Container, FormGroup, Label, Input, Form, Col} from "reactstrap";
import Snake from "./snake";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";


const App = () => {

    const [gameType, setGameType] = useState("circular");
    const [gameLevel, setGameLevel] = useState("1");

    return (
        <div className="App">
            <Container fluid>
                <div className="App-header">
                    <div className="App-header-title">
                        Snake
                    </div>
                    <div>
                        <Form>
                            <FormGroup row>
                                <Label for="gameType" sm={{size: 3, offset: 3}}>Game type: <b>{gameType}</b></Label>
                                <Col sm={2}>
                                    <Input
                                        type="select"
                                        id="gameType"
                                        name="gameType"
                                        onChange={(e) => setGameType(e.target.value)}
                                        className="App-selector">
                                        <option defaultChecked>circular</option>
                                        <option>hard wall</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="levelInput" sm={{size: 3, offset: 3}}>Game level: <b>{gameLevel}</b></Label>
                                <Col sm={2}>
                                    <Input
                                        type="select"
                                        id="levelInput"
                                        name="levelInput"
                                        onChange={(e) => setGameLevel(e.target.value)}
                                        className="App-selector">
                                        <option defaultChecked>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                        </Form>
                    </div>
                </div>
            </Container>
            <Snake level={gameLevel} mode={gameType} />
            <Container fluid>
                <div className="App-instructions">
                    To play the game, use your arrow keys or click on the buttons above.<br />
                    Set the level and the game type, then click on &quot;New Game&quot; button to start!
                </div>
            </Container>
        </div>
    );
};


export default App;
