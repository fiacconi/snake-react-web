/* eslint-disable functional/immutable-data */
/* eslint-disable functional/no-expression-statement */
import { canvasProperties, directions, gameModes } from "./constants";

const cleanCanvasAction = ({ctx}) => {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    return { ctx };
};

const drawHardWallAction = ({ctx, mode}) => {

    if (mode === gameModes.HARD_WALL) {
        ctx.beginPath();
        ctx.strokeRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.lineWidth = 2 * canvasProperties.SCALE;
        ctx.strokeStyle = "rgba(0,0,0,0.5)";
        ctx.closePath();
    }

    return { ctx };
};

const drawGameOverAction = ({ctx}) => {
    
    ctx.beginPath();
    ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.fillStyle = "rgba(178, 189, 8, 0.6)";
    ctx.fill();
    ctx.closePath();

    ctx.beginPath();
    ctx.textAlign = "center";
    ctx.font = "20px Lato";
    ctx.fillStyle = canvasProperties.FILL_STYLE;
    ctx.fillText("GAME OVER", Math.floor(ctx.canvas.width * 0.5), Math.floor(ctx.canvas.height * 0.5) + 10);
    ctx.closePath();

    return { ctx };
};

const drawOnCanvas = (ctx, drawer) => {
    ctx.beginPath();
    drawer(ctx);
    ctx.fillStyle = canvasProperties.FILL_STYLE;
    ctx.fill();
    ctx.closePath();

    return { ctx };
};

const foodDrawer = (foodPosition) => {
    const { x, y } = foodPosition;
    const foodDisplacement = [[1, 2], [2, 1], [2, 3], [3, 2]];

    return (ctx) => foodDisplacement.forEach(
        disp => ctx.rect(
            (x + disp[0]) * canvasProperties.SCALE, 
            (y + disp[1]) * canvasProperties.SCALE, 
            canvasProperties.SCALE, 
            canvasProperties.SCALE
        )
    );
};

const bannerDrawer = () => {
    return (ctx) => canvasProperties.BANNER.forEach((row, iy) => {
        row.forEach((ix) => {
            ctx.rect(ix * canvasProperties.SCALE, iy * canvasProperties.SCALE, canvasProperties.SCALE, canvasProperties.SCALE);
        });
    });
};

const snakeHeadDrawer = (snake, openMouth) => {

    const head = snake[0];

    const headDisplacement = 
        (head.direction === directions.RIGHT) ? [[0, 0], [0, 2], [1, 1], [1, 2]] : 
            (head.direction === directions.UP)    ? [[0, 3], [2, 3], [1, 2], [2, 2]] :
                (head.direction === directions.DOWN)  ? [[0, 0], [2, 0], [1, 1], [2, 1]] :
                    (head.direction === directions.LEFT)  ? [[3, 0], [3, 2], [2, 1], [2, 2]] : [];
    
    const mouthDisplacement = 
        (head.direction === directions.RIGHT) ? (openMouth ? [[2, 0], [2, 3]] : [[2, 1], [2, 2]]) :
            (head.direction === directions.UP)    ? (openMouth ? [[0, 1], [3, 1]] : [[1, 1], [2, 1]]) :
                (head.direction === directions.DOWN)  ? (openMouth ? [[0, 2], [3, 2]] : [[1, 2], [2, 2]]) :
                    (head.direction === directions.LEFT)  ? (openMouth ? [[1, 0], [1, 3]] : [[1, 1], [1, 2]]) : [];
    
    return (ctx) => {
        headDisplacement.forEach(disp => ctx.rect(
            (head.x + disp[0]) * canvasProperties.SCALE, 
            (head.y + disp[1]) * canvasProperties.SCALE, 
            canvasProperties.SCALE, 
            canvasProperties.SCALE
        ));
        mouthDisplacement.forEach(disp => ctx.rect(
            (head.x + disp[0]) * canvasProperties.SCALE, 
            (head.y + disp[1]) * canvasProperties.SCALE, 
            canvasProperties.SCALE, 
            canvasProperties.SCALE
        ));
    };
};

const snakeTailDrawer = (snake) => {

    const tail = snake[snake.length - 1];

    const tailDisplacement = 
        (tail.direction === directions.RIGHT) ? [[3, 1], [3, 2], [2, 2]] :
            (tail.direction === directions.UP)    ? [[1, 0], [2, 0], [2, 1]] :
                (tail.direction === directions.LEFT)  ? [[0, 1], [0, 2], [1, 1]] :
                    (tail.direction === directions.DOWN)  ? [[1, 3], [2, 3], [1, 2]] : [];
    
    return (ctx) => {
        tailDisplacement.forEach(disp => ctx.rect(
            (tail.x + disp[0]) * canvasProperties.SCALE, 
            (tail.y + disp[1]) * canvasProperties.SCALE, 
            canvasProperties.SCALE, 
            canvasProperties.SCALE
        ));
    };
};

const snakeBodyDrawer = (snake) => {

    const body = snake.slice(1, -1);

    return (ctx) => {
        body.forEach((block, i) => {

            const nextBlockDirection = snake[i].direction;
    
            const sameDirectionDisplacement = 
                (block.direction === directions.LEFT || block.direction === directions.RIGHT) ? 
                    [[0, 1], [0, 2], [1, 1], [2, 2], [3, 1], [3, 2]].concat(block.fill ? [[1, 0], [2, 0], [1, 3], [2, 3]] : []) : 
                    [[1, 0], [2, 0], [2, 1], [1, 2], [1, 3], [2, 3]].concat(block.fill ? [[0, 1], [0, 2], [3, 1], [3, 2]] : []);
            
            const differentDirectionDisplacement = 
            (block.direction === directions.RIGHT) ? [[0, 1], [0, 2]].concat(
                (nextBlockDirection === directions.UP) ? [[1, 2], [2, 1], [1, 0], [2, 0]].concat(block.fill ? [[3, 1], [3, 2], [1, 3], [2, 3]] : []) :
                    (nextBlockDirection === directions.DOWN) ? [[1, 1], [2, 2], [1, 3], [2, 3]].concat(block.fill ? [[3, 1], [3, 2], [1, 0], [2, 0]]: []) : []   
            ) : (block.direction === directions.UP) ? [[1, 3], [2, 3]].concat(
                (nextBlockDirection === directions.RIGHT) ? [[1, 2], [2, 1], [3, 1], [3, 2]].concat(block.fill ? [[1, 0], [2, 0], [0, 1], [0, 2]] : []) :
                    (nextBlockDirection === directions.LEFT) ? [[1, 1], [2, 2], [0, 1], [0, 2]].concat(block.fill ? [[1, 0], [2, 0], [3, 1], [3, 2]] : []) : []
            ) : (block.direction === directions.LEFT) ? [[3, 1], [3, 2]].concat(
                (nextBlockDirection === directions.UP) ? [[2, 2], [1, 1], [1, 0], [2, 0]].concat(block.fill ? [[0, 1], [0, 2], [1, 3], [2, 3]] : []) :
                    (nextBlockDirection === directions.DOWN) ? [[2, 1], [1, 2], [1, 3], [2, 3]].concat(block.fill ? [[0, 1], [0, 2], [1, 0], [2, 0]] : []) : []
            ) : (block.direction === directions.DOWN) ? [[1, 0], [2, 0]].concat(
                (nextBlockDirection === directions.RIGHT) ? [[1, 1], [2, 2], [3, 1], [3, 2]].concat(block.fill ? [[1, 3], [2, 3], [0, 1], [0, 2]] : []) :
                    (nextBlockDirection === directions.LEFT) ? [[2, 1], [1, 2], [0, 1], [0, 2]].concat(block.fill ? [[1, 3], [2, 3], [3, 1], [3, 2]] : []) : []
            ) : [];
    
            const bodyDisplacement = (block.direction === nextBlockDirection) ? sameDirectionDisplacement : differentDirectionDisplacement;
            
            bodyDisplacement.forEach(disp => ctx.rect(
                (block.x + disp[0]) * canvasProperties.SCALE, 
                (block.y + disp[1]) * canvasProperties.SCALE, 
                canvasProperties.SCALE, 
                canvasProperties.SCALE
            ));
        });
    };
};

const snakeDrawer = (snake, openMouth) => {
    const head = snakeHeadDrawer(snake, openMouth);
    const body = snakeBodyDrawer(snake);
    const tail = snakeTailDrawer(snake);

    return (ctx) => {
        head(ctx);
        body(ctx);
        tail(ctx);
    };
};


export const drawUtils = {
    cleanCanvasAction,
    drawHardWallAction,
    drawGameOverAction,
    drawBannerAction:   ({ctx})                   => drawOnCanvas(ctx, bannerDrawer()),
    drawFoodAction:     ({ctx, foodPosition})     => drawOnCanvas(ctx, foodDrawer(foodPosition)),
    drawSnakeAction:    ({ctx, snake, openMouth}) => drawOnCanvas(ctx, snakeDrawer(snake, openMouth)),
};
