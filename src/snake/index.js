import React, { useRef, useEffect } from "react";
import { Container } from "reactstrap";

import { canvasProperties, gameStates, keyboard } from "./constants";
import { drawUtils } from "./draw";
import { getInputHandler } from "./handlers";
import { gameLogic, gameStateActions } from "./logic";
import { retrieveBestScoreLocalStorage, persistBestScoreLocalStorage} from "./utils";

import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";


// eslint-disable-next-line react/prop-types
const Snake = ({level, mode}) => {

    const canvasRef = useRef(null);
    const scoreRef = useRef(null);
    const bestScoreRef = useRef(null);
    const requestAnimationRef = useRef(undefined);

    const gameState = { state: {} };
    const inputHandler = getInputHandler(gameState);

    const haltGameLoop = () => {

        window.cancelAnimationFrame(requestAnimationRef.current);

        // eslint-disable-next-line functional/immutable-data
        bestScoreRef.current.innerText = `${Math.max(parseInt(bestScoreRef.current.innerText), parseInt(scoreRef.current.innerText))}`;
        persistBestScoreLocalStorage(mode, level, bestScoreRef.current.innerText);

        // eslint-disable-next-line functional/immutable-data
        scoreRef.current.innerText = "0";
    };

    const gameLoop = (time) => {

        if (!gameState.state.time) {
            // eslint-disable-next-line functional/immutable-data
            gameState.state = gameLogic.evolveGameState([gameStateActions.setNewTimeActionGenerator(time)], gameState.state);
        }

        if ((time - gameState.state.time) > gameState.state.frameFreq) {

            const preliminaryActions = [
                gameStateActions.updateDirectionAction,
                gameStateActions.testIsSnakeWrapOnItselfAction,
                gameStateActions.testSnakeHitTheWallAction,
                drawUtils.cleanCanvasAction,
                drawUtils.drawHardWallAction
            ];

            // eslint-disable-next-line functional/immutable-data
            gameState.state = gameLogic.evolveGameState(preliminaryActions, gameState.state);

            const gameOverActions = [
                drawUtils.drawFoodAction,
                drawUtils.drawSnakeAction,
                drawUtils.drawGameOverAction
            ];

            const gameOnActions = [
                gameStateActions.evolveSnakeAction,
                gameStateActions.testIsOpenMouthAction,
                gameStateActions.testIsSnakeEatingAction,
                gameStateActions.updateFoodPositionWhenSnakeEatsAction,
                drawUtils.drawFoodAction,
                drawUtils.drawSnakeAction
            ];

            // eslint-disable-next-line functional/immutable-data
            gameState.state = gameLogic.evolveGameState(
                (gameState.state.status === gameStates.GAME_OVER) ? gameOverActions : gameOnActions, 
                gameState.state
            );

            if (gameState.state.status === gameStates.GAME_OVER) {
                haltGameLoop();
                return;
            }

            if (gameState.state.isEating) {
                // eslint-disable-next-line functional/immutable-data
                scoreRef.current.innerText = `${parseInt(scoreRef.current.innerText) + canvasProperties.FOOD_EATING_SCORE}`;
            }

            // eslint-disable-next-line functional/immutable-data
            gameState.state = gameLogic.evolveGameState([gameStateActions.setNewTimeActionGenerator(time)], gameState.state);
        }

        // eslint-disable-next-line functional/immutable-data
        requestAnimationRef.current = window.requestAnimationFrame(gameLoop);
    };

    const startGameLoop = () => {
        // eslint-disable-next-line functional/immutable-data
        gameState.state = gameLogic.initGameState({level, mode, ctx: canvasRef.current.getContext("2d")});
        gameLoop();
    };

    useEffect(() => {

        const ctx = canvasRef.current.getContext("2d");
        // eslint-disable-next-line functional/immutable-data
        gameState.state = gameLogic.initGameState({level, mode, ctx});        
        // eslint-disable-next-line functional/immutable-data
        gameState.state = gameLogic.evolveGameState([drawUtils.drawBannerAction], gameState.state);
        // eslint-disable-next-line functional/immutable-data
        bestScoreRef.current.innerText = `${retrieveBestScoreLocalStorage(mode, level)}`;

        window.document.addEventListener("keydown", inputHandler);

        return () => {
            window.document.removeEventListener("keydown", inputHandler);

            if (requestAnimationRef.current) {
                haltGameLoop();
                // eslint-disable-next-line functional/immutable-data
                gameState.state = gameLogic.evolveGameState([drawUtils.cleanCanvasAction], gameState.state);
            }
        };
    });

    return (
        <Container fluid>
            <div >
                <button className="App-button" onClick={() => {
                    if (requestAnimationRef.current) {
                        haltGameLoop();
                    }
                    startGameLoop();
                }}>New Game</button>
            </div>
            <div className="App-score">
                Score: <span ref={scoreRef}>0</span>&emsp;&emsp;Best score: <span ref={bestScoreRef}>0</span>
            </div>
            <div>
                <canvas 
                    id="gameCanvas" 
                    ref={canvasRef} 
                    width={canvasProperties.CANVAS_WIDTH} 
                    height={canvasProperties.CANVAS_HEIGHT} />
            </div>
            <div className="App-joypad-container">
                <div>
                    <button className="App-button" onClick={() => inputHandler({key: keyboard.ARROW_UP})}>Up</button>
                </div>
                <div>
                    <button className="App-button" onClick={() => inputHandler({key: keyboard.ARROW_LEFT})}>Left</button>
                    <button className="App-button" onClick={() => inputHandler({key: keyboard.ARROW_RIGHT})}>Right</button>
                </div>
                <div>
                    <button className="App-button" onClick={() => inputHandler({key: keyboard.ARROW_DOWN})}>Down</button>
                </div>
            </div>
        </Container>
    );
};


export default Snake;