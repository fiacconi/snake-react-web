import {canvasProperties, directions, baseBestScoreObj} from "./constants";

const getFrameInterval = (level) => {
    const levelNumber = (typeof level === "string") ? parseInt(level) : level;
    return 1000 * (6 - levelNumber) / canvasProperties.BASE_FPS;
};

const getDirectionMultipliers = (direction) => {
    const xMult = (direction === directions.UP || direction === directions.DOWN) ? 0 : ((direction === directions.RIGHT) ? 1 : -1);
    const yMult = (direction === directions.RIGHT || direction === directions.LEFT) ? 0 : ((direction === directions.DOWN) ? 1 : -1);
    return [xMult, yMult];
};

const initNewRandomPosition = () => ({
    x: Math.floor(Math.random() * canvasProperties.NOKIA_SCREEN_WIDTH / 4) * 4, 
    y: Math.floor(Math.random() * canvasProperties.NOKIA_SCREEN_HEIGHT / 4) * 4
});


const retrieveBestScoreLocalStorage = (mode, level) => {
    const stringifiedBestScore = window.localStorage.getItem("snakeBestScore");
    const bestScoreObj = (stringifiedBestScore) ? JSON.parse(stringifiedBestScore) : baseBestScoreObj;
    return (`${mode}_${level}` in bestScoreObj) ? bestScoreObj[`${mode}_${level}`] : "0";
};


const persistBestScoreLocalStorage = (mode, level, score) => {
    const stringifiedBestScore = window.localStorage.getItem("snakeBestScore");
    const bestScoreObj = (stringifiedBestScore) ? JSON.parse(stringifiedBestScore) : baseBestScoreObj;
    const updatedBestScore = {
        ...bestScoreObj, 
        [`${mode}_${level}`]: (`${mode}_${level}` in bestScoreObj) ? Math.max(score, bestScoreObj[`${mode}_${level}`]).toString() : score.toString()
    };
    window.localStorage.setItem("snakeBestScore", JSON.stringify(updatedBestScore));
};


export {
    getFrameInterval,
    getDirectionMultipliers, 
    initNewRandomPosition,
    retrieveBestScoreLocalStorage,
    persistBestScoreLocalStorage
};
