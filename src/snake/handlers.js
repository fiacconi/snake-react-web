import {directions, keyboard} from "./constants";


export const getInputHandler = (gameState) => {

    const localReferenceToGameState = gameState;

    return (e) => {

        const direction = localReferenceToGameState.state.direction;

        // eslint-disable-next-line functional/immutable-data
        localReferenceToGameState.state.directionUpdate = 
            (direction === directions.RIGHT || direction === directions.LEFT) ? (
                (e.key === keyboard.UP || e.key === keyboard.ARROW_UP) ? directions.UP : 
                    (e.key === keyboard.DOWN || e.key === keyboard.ARROW_DOWN) ? directions.DOWN : 
                        direction
            ) : (direction === directions.UP || direction == directions.DOWN) ? (
                (e.key === keyboard.RIGHT || e.key === keyboard.ARROW_RIGHT) ? directions.RIGHT :
                    (e.key === keyboard.LEFT || e.key === keyboard.ARROW_LEFT) ? directions.LEFT :
                        direction
            ) : direction;
    };
};