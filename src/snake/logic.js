import { initNewRandomPosition, getFrameInterval, getDirectionMultipliers } from "./utils";
import { directions, canvasProperties, gameStates, gameModes } from "./constants";


const createNewSnake = () => {
    const y = canvasProperties.NOKIA_SCREEN_HEIGHT / 2;
    const direction = directions.RIGHT;
    const fill = false;
    const snake = canvasProperties.INITIAL_SNAKE_POS.map((x) => ({ x, y, direction, fill }));
    return snake;
};

const createNewRandomFood = (snake) => {

    // eslint-disable-next-line functional/no-let
    let newCandidate = initNewRandomPosition();
    
    // eslint-disable-next-line functional/no-loop-statement
    while (snake.map(x => ((x.x === newCandidate.x) && (x.y === newCandidate.y))).reduce((acc, x) => acc || x, false)) {
        newCandidate = initNewRandomPosition();
    }

    return newCandidate;
};

const initGameState = ({level, mode, ctx}) => {
    
    const snake = createNewSnake();
    const foodPosition = createNewRandomFood(snake);
    const frameFreq = getFrameInterval(level);

    const baseState = {
        status: gameStates.GAME_ON,
        direction: directions.RIGHT,
        directionUpdate: directions.RIGHT,
        openMouth: false,
        isEating: false,
        time: undefined
    };
    
    return {
        ...baseState,
        level,
        mode,
        ctx,
        snake,
        foodPosition,
        frameFreq
    };
};

const testIsOpenMouthAction = ({snake, foodPosition}) => {
    const head = snake[0];
    const openMouth = 
        (head.direction === directions.RIGHT) ? ((foodPosition.x - head.x === canvasProperties.SCALE) && (foodPosition.y === head.y)) : 
            (head.direction === directions.UP)    ? ((foodPosition.y - head.y === -canvasProperties.SCALE) && (foodPosition.x === head.x)) :
                (head.direction === directions.LEFT)  ? ((foodPosition.x - head.x === -canvasProperties.SCALE) && (foodPosition.y === head.y)) :
                    (head.direction === directions.DOWN)  ? ((foodPosition.y - head.y === canvasProperties.SCALE) && (foodPosition.x === head.x)) : 
                        false;
    return { openMouth };
};

const testIsSnakeEatingAction = ({snake, foodPosition}) => {
    const head = snake[0];
    const isEating = (foodPosition.x === head.x) && (foodPosition.y === head.y);
    return { isEating };
};

const testIsSnakeWrapOnItselfAction = ({snake, direction, status}) => {

    const {x: currentX, y: currentY} = snake[0];

    const predictedHead = 
        (direction === directions.RIGHT) ? {x: currentX + canvasProperties.SCALE, y: currentY} : 
            (direction === directions.UP) ? {x: currentX, y: currentY - canvasProperties.SCALE} : 
                (direction === directions.LEFT) ? {x: currentX - canvasProperties.SCALE, y: currentY} : 
                    (direction === directions.DOWN) ? {x: currentX, y: currentY + canvasProperties.SCALE} : 
                        {x: currentX, y: currentY};
    
    const isWrap = snake.slice(1, -1).map(block => (block.x === predictedHead.x) && (block.y === predictedHead.y)).reduce((acc, val) => acc || val, false);
    const newGameStatus = isWrap ? gameStates.GAME_OVER : gameStates.GAME_ON;

    return { status: (status === gameStates.GAME_OVER) ? gameStates.GAME_OVER : newGameStatus };
};

const testSnakeHitTheWallAction = ({snake, direction, mode, status}) => {

    const willSnakeHitTheWall = 
        (direction === directions.RIGHT) ? ((snake[0].x + canvasProperties.SCALE) === canvasProperties.NOKIA_SCREEN_WIDTH) :
            (direction === directions.UP) ? ((snake[0].y - canvasProperties.SCALE) < 0) :
                (direction === directions.LEFT) ? ((snake[0].x - canvasProperties.SCALE) < 0) :
                    (direction === directions.DOWN) ? ((snake[0].y + canvasProperties.SCALE) === canvasProperties.NOKIA_SCREEN_HEIGHT) : 
                        false;

    const newGameStatus = (mode === gameModes.CIRCULAR) ? gameStates.GAME_ON : (willSnakeHitTheWall) ? gameStates.GAME_OVER : gameStates.GAME_ON;
    
    return { status: (status === gameStates.GAME_OVER) ? gameStates.GAME_OVER : newGameStatus };
};

const evolveSnakeAction = ({ snake, direction, mode, isEating}) => {

    const [snakeHead, ...snakeBody] = snake;
    const [xMult, yMult] = getDirectionMultipliers(direction);

    const baseBlock = {
        direction,
        x: snakeHead.x + xMult * canvasProperties.SCALE,
        y: snakeHead.y + yMult * canvasProperties.SCALE,
        fill: false
    };

    const newSnakeBlock = (mode === gameModes.CIRCULAR) ? {
        ...baseBlock, 
        x: ((baseBlock.x > 0) ? baseBlock.x : (canvasProperties.NOKIA_SCREEN_WIDTH + baseBlock.x)) % canvasProperties.NOKIA_SCREEN_WIDTH,
        y: ((baseBlock.y > 0) ? baseBlock.y : (canvasProperties.NOKIA_SCREEN_HEIGHT + baseBlock.y)) % canvasProperties.NOKIA_SCREEN_HEIGHT,
    } : baseBlock;

    const tmpSnake = (isEating) ? [newSnakeBlock, {...snakeHead, fill: true}, ...snakeBody] : [newSnakeBlock, snakeHead, ...(snakeBody.slice(0, -1))];

    const newSnakeBody = tmpSnake.slice(0, -2);
    const [tail1, tail2] = tmpSnake.slice(-2);

    return {snake: [...newSnakeBody, tail1, {...tail2, direction: tail1.direction}]};
};

const setNewTimeActionGenerator = (newTime) => {
    return () => ({time: newTime});
};

const updateDirectionAction = ({directionUpdate}) => {
    return {
        direction: directionUpdate
    };
};

const updateFoodPositionWhenSnakeEatsAction = ({isEating, foodPosition, snake}) => { 
    return {
        foodPosition: (isEating) ? createNewRandomFood(snake) : foodPosition 
    };
};

const evolveGameState = (actions, initState) => {
    return actions.reduce((prevState, action) => {
        return {
            ...prevState, 
            ...action(prevState)
        };
    }, initState);
};


export const gameStateActions = {
    evolveSnakeAction,
    testIsOpenMouthAction,
    testIsSnakeEatingAction,
    testIsSnakeWrapOnItselfAction,
    testSnakeHitTheWallAction,
    setNewTimeActionGenerator,
    updateDirectionAction,
    updateFoodPositionWhenSnakeEatsAction
};

export const gameLogic = {
    initGameState,
    evolveGameState
};